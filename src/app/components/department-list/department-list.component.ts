import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../services/debarment.service';
import { IDepartment } from '../../core/interfaces/department.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss']
})
export class DepartmentListComponent implements OnInit {
  destroySubject$: Subject<void> = new Subject();
  departments: IDepartment[];
  departmentsForDisplay: IDepartment[];
  constructor(private departmentService: DepartmentService) { }

  ngOnInit() {
    this.departmentService.getDepartments()
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((departments) => {
          this.departments = departments;
          this.departmentsForDisplay =  departments;
        });
  }

    searchTodoBeKeyWord(event: string): void {
        this.departmentsForDisplay = [];
        if (event.length) {
            this.departmentsForDisplay = this.departments.filter(item => {
                if (item) {
                    return item.departmentName.toLowerCase().includes(event.toLowerCase())
                        || item.apiKey.toLowerCase().includes(event.toLowerCase())
                        || item.departmentContactPerson.name.toLowerCase().includes(event.toLowerCase())
                        || item.departmentContactPerson.email.toLowerCase().includes(event.toLowerCase())
                        || item.departmentContactPerson.phone.toLowerCase().includes(event.toLowerCase());
                } else {
                    return false;
                }
            });
        } else {
            this.departmentsForDisplay = this.departments;
        }
    }

}
