import {Component, Input, OnInit} from '@angular/core';
import {IDepartment} from '../../core/interfaces/department.interface';

@Component({
  selector: 'app-department-item',
  templateUrl: './department-item.component.html',
  styleUrls: ['./department-item.component.scss']
})
export class DepartmentItemComponent implements OnInit {
  @Input() department: IDepartment;
  constructor() { }

  ngOnInit() {
  }
}
