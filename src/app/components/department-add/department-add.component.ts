import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DepartmentService } from '../../services/debarment.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.scss']
})
export class DepartmentAddComponent implements OnInit {
  destroySubject$: Subject<void> = new Subject();
  departmetForm: FormGroup;

  constructor(private fb: FormBuilder, private departmentService: DepartmentService, private router: Router) { }

  ngOnInit() {
    this.departmetForm = this.fb.group({
        departmentName: [null],
        apiKey: [null],
        departmentContactPerson: this.fb.group({
            name: [null],
            email: [null],
            phone: [null]
        })
    });
  }

  onSubmit() {
      this.departmentService.addDepartment(this.departmetForm.value)
          .pipe(takeUntil(this.destroySubject$))
          .subscribe(() => {
            this.router.navigate(['departments']);
          });
  }
}
