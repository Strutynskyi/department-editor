import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {fromEvent} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Router} from '@angular/router';


@Component({
    selector: 'app-search-panel',
    templateUrl: './serch-panel.component.html',
    styleUrls: ['./serch-panel.component.scss'],
})
export class SearchPanelComponent implements OnInit, AfterViewInit {
    @ViewChild('searchInput') searchInput: ElementRef;
    @Output() searchEvent = new EventEmitter<string>()
    @Input() elementsArr: any[];
    keyWord: string = null;

    constructor(private router: Router) {
    }

    conductSearch(keyWord: string) {
        this.searchEvent.emit(keyWord);
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(debounceTime(700),
                map((event: any) => event.target.value),
                distinctUntilChanged())
            .subscribe(res => {
                this.conductSearch(res);
            });
    }

    navigeteToAddDepartment() {
        this.router.navigate(['add-department']);
    }
}
