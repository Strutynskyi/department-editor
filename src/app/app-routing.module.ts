import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DepartmentListComponent} from './components/department-list/department-list.component';
import {DepartmentAddComponent} from './components/department-add/department-add.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'departments'
    },

    {
        path: 'departments',
        component: DepartmentListComponent
    },
    {
        path: 'add-department',
        component: DepartmentAddComponent
    },

    {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
