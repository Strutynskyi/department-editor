import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { DepartmentListComponent } from './components/department-list/department-list.component';
import { DepartmentItemComponent } from './components/department-item/department-item.component';
import { DepartmentEditComponent } from './components/department-edit/department-edit.component';
import { DepartmentModalComponent } from './components/department-modal/department-modal.component';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { StepperComponent } from './components/stepper/stepper.component';
import {SearchPanelComponent} from './components/search-panel/serch-panel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DepartmentAddComponent } from './components/department-add/department-add.component';

@NgModule({
  declarations: [
    AppComponent,
    DepartmentListComponent,
    DepartmentItemComponent,
    DepartmentEditComponent,
    DepartmentModalComponent,
    AppHeaderComponent,
    StepperComponent,
    SearchPanelComponent,
    DepartmentAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
