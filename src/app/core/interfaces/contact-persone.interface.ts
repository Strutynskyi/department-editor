export interface IDepartmentContactPersone {
    name: string;
    email: string;
    phone: string;
}
