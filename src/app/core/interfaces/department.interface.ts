import {IDepartmentContactPersone} from './contact-persone.interface';

export interface IDepartment {
    departmentName: string;
    apiKey: string;
    departmentContactPerson: IDepartmentContactPersone;
}
