import { TestBed } from '@angular/core/testing';

import { DebarmentService } from './debarment.service';

describe('DebarmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DebarmentService = TestBed.get(DebarmentService);
    expect(service).toBeTruthy();
  });
});
