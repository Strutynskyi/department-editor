import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {IDepartment} from '../core/interfaces/department.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
    readonly endpoint = environment.api;
  constructor(private http: HttpClient) { }


    getDepartments(): Observable<IDepartment[]> {
        return this.http.get<IDepartment[]>(`${this.endpoint}/departments`);
    }

    addDepartment(todo: IDepartment): Observable<IDepartment> {
        return this.http.post<IDepartment>(`${this.endpoint}/departments`, todo);
    }

    editDepartment(id: number, todo: IDepartment): Observable<IDepartment> {
        return this.http.put<IDepartment>(`${this.endpoint}/departments/${id}`, todo);
    }

    deleteDepartment(id: number): Observable<IDepartment> {
        return this.http.delete<IDepartment>(`${this.endpoint}/departments/${id}`);
    }

}
